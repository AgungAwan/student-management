// import logo from './logo.svg';
// import './App.css';
import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Student from './pages/Student';
import Addstudent from './pages/Addstudent';
import Editstudent from './pages/Editstudent'
import Navbar from './components/Navbar';
import NotFoundPage from './pages/errors/404-page';

function App() {
  return (
    <Router>
      <Navbar />
      <Switch>
        <Route exact path="/" component={Student} />
        <Route exact path="/add-student" component={Addstudent} />
        <Route exact path="/edit-student/:id" component={Editstudent} />
        <Route component={NotFoundPage} />
      </Switch>
    </Router>
  );
}

export default App;
