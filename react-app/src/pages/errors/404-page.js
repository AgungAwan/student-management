import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NotFoundPage extends Component {
    render() {
        return(
            <div style={{padding: "40px 15px", textAlign: "center"}}>
                <h3>404 page not found</h3>
                <p>We are sorry but the page you are looking for does not exist.</p>
                <Link to={`/`} type="button" class="btn btn-outline-primary">Take Me Home</Link>
            </div>
        );
    }
}

export default NotFoundPage
